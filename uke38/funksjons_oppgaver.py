# Oppgave 1
# Lag en funksjon skriv(). Det eneste den skal gjøre
# er å skrive ut tallet 42 til terminalen.
# Denne funksjonen kalles ikke for å gjøre noe som
# den skal returnere til oss, men for noe den gjør selv.

# Kall funksjonen med skriv() akkurat som om du skrev print()

# Oppgave 2 
# Lag en funksjon skriv_dette(dette). Den skal ta imot
# en parameter, så skal denne skrives ut i terminalen

# Kall funksjonen med skriv_dette('ikke dette, men dette')


# Nå skal dere jobbe med et annet aspekt ved funksjoner,
# og det er at vi bevisst kan returnere noe fra dem.
# Dette er det viktig at dere forstår. På forelesning
# ba jeg dere om å regne ut hvor mange tegn det var i
# fornavnet, og så skrive det i chat. Dere returnerte det
# til meg, slik at jeg kunne bruke det videre hvis jeg ville
# Funksjoner som ikke har en 'return <noe>' vil bare
# returnere 'NoneType' elns.
# Den eksisterende funksjonen print er en funksjon som de over
# den gjør en jobb og så returnerer den ikke noe av verdi.
# Det er grunnen til at denne koden feiler (verdi = None)
verdi = print(2+4)
print(verdi)


# Oppgave 3 - returverdi
# Lag funksjonen beregn_areal_av_rektangel(lengde, bredde).
# Funksjonen skal beregne arealet av rektangelet, og så
# returnere arealet.

# Kall funksjonen med beregn_areal_av_rektangel(4, 9)

# Oppgave med while OG kall av annen, selvlaget funkjon
# Og mulighet til å bruke break om en vil

# Oppgave 4 - en funksjon kan bruke en annen funksjon!
# Lag en funksjon stort_rektangel(). Denne skal spørre
# brukeren om lengde og bredde til et rektangel. Hvis
# arealet av rektangelet er mindre enn 100, da skal den
# spørre igjen. Hvis areal er lik eller større enn 100,
# da skal den returnere lengde og bredde
# Man returnerer både a og b slik: return a, b
# Og HUSK: du har allerede funksjonen
# beregn_areal_av_rektangel. Kan du bruke denne?

# Kall funksjonen med stort_rektangel() Får du ikke noe
# skrevet ut? Kanskje du må skrive ut svaret du får?