# Multiplikasjon av 2 matriser i standard Python ved hjelp av 2D lister
def matrisemult(a,b):
    resultat = []
    for i in range(len(a)):
        liste = []
        for j in range(len(b[0])):
            el = 0
            for x in range(len(b)):
                print(f'{a[i][x]}*{b[x][j]}',end=" ")
                el += a[i][x]*b[x][j]
            print()
            liste.append(el)       
        resultat.append(liste)
    return resultat

a = [[11,12,13,14,15],[16,17,18,19,20],[21,22,23,24,25]]
b = [[2,4,8],[6,8,10],[1,2,3],[2,3,4],[4,3,2]]
if len(a[0]) == len(b):
    print(matrisemult(a,b))
else:
    print('Det går dessverre ikke.')