
def roter_streng(streng, antall):
    tmp_streng = ""
    for c in streng:
        tmp_streng += chr(ord(c) + antall)
    return tmp_streng


streng = input("Streng: ")
antall = int(input("Rotasjoner: "))

print(f'{streng} rotert {antall} ganger blir "{roter_streng(streng, antall)}".')
